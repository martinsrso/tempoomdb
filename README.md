## Premissas assumidas
Foi assumida um ordenaćão por vez (alfabética ou avaliaćão) e se dá da esquerda para direita.
Também foi desabilitado os botões enquanto o usuário não digitar nada no campo de título.
Clicando na busca ele busca pelo título e caso exista ano utiliza-se o ano em conjunto com o 
título. Quando a API não retorna um valor válido exibe-se uma alerta no browser sobre a 
falta de dados.
Por ter sido feito de forma contínua e soziho o git foi pouco explorado apenas utilizando funcões
básicas de commit, push, add e clone. 
As partes mais importantes do código estão comentados bem como alguns arquivos de configuracão.

## Para executar

Clone o reposiótiorio para uma pasta de sua preferência.

Para executar o projeto faça:

### `npm install` ou `yarn install` 
### `npm start` ou `yarn start`

Quando o aplicativo estiver executando em modo de desenvolvimento.<br>
Abra [http://localhost:3000](http://localhost:3000) para ver em seu brwoser (Recomendo o Google Chrome).

### `npm test`

Devido a problemas na configuração do jest para funcionar com o `Redux-Thunk` não foi feito nenhum teste unitário.

## Deciões de projeto
Todo o projeto foi de forma funcional e `stateless`. Foi utilizado para melhor aproveitando 
do consumo da API o `redux-thunk` que é um middleware para criar acões assincronas com o estado da 
aplicacão.

Não foi adotado nenhum design pattern como Atomic, por considerar que seria mais trabalhosa 
utilizar para este projeto.

Sobre a estrutura ficou como mostra na árvore de arquivos, separando apenas em componentes e 
containeres. 

## Implementacao

```
├── enzyme.conf.js
├── node_modules
├── package.json
├── src
    ├── actions
    │   └── actions.js
    ├── components
    │   ├── MovieCardComponent.jsx
    │   ├── MovieComponent.jsx
    │   ├── MovieListComponent.jsx
    │   └── SearchComponent.jsx
    ├── containers
    │   ├── MovieContainer.js
    │   └── SearchContainer.js
    ├── index.jsx
    ├── reducers
    │   └── reducers.js
    ├── routes.jsx
    └── template.html
├── test
├── webpack.config.js
└── yarn.lock
```

A estrutura do projeto está definida como mostra as árvore. 
Todo código e lógica está na pasta `src` que contém outras pastas.

### Componentes
Foi criado 4 componentes 3 para responsáveis pela a exibićão da listagem dos filmes 
e da tela onde encontra-se os detalhes de um determinado filme.

### Containers 
Os `containers` estão organizados para criar a conexão com o redux e passar o estado como
propriedade para os componentes filhos a esse componentes.\

### Actions 
Contém as acões para atualizar o estado.

### Reducers 
Define o estado inicial e mapeia as acões por `payloads`.

Obs: Qualquer eventual dúvida estou a disposićão.