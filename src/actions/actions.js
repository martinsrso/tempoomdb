import axios from 'axios'

// Para guardar a entrada no input referente ao título
export const changeTitle = title => ({ type: 'CHANGE_TITLE', payload: title })

// Para guardar a entrada no input referente ao ano
export const changeYear = year => ({ type: 'CHANGE_YEAR', payload: year })

export const clean = () => ({type: 'CLEAN'})

export const sort = (sortAttr) => (dispatch, getState) => {
  const { movies } = getState()
  switch (sortAttr[0]) {
    case "rating":
      if (sortAttr[2])
        movies.sort((a, b) => (a.rating > b.rating) ? 1 : -1)
      else
        movies.sort((a, b) => (a.rating < b.rating) ? 1 : -1)
      break
    case "alfabetica":
    default:
      if (sortAttr[1])
        movies.sort((a, b) => (a.title > b.title) ? 1 : -1)
      else
        movies.sort((a, b) => (a.title < b.title) ? 1 : -1)
      break
  }

  dispatch({
    type: 'SORT',
    payload: {
      movies,
      sortAttr
    }
  })
}

export const search = () => (dispatch, getState) => {
  const { title, year } = getState()

  let urlTitle = title == '' ? `` : `&s=${title}`
  let urlYear = year == '' ? `` : `&y=${year}`

  axios.get(`http://www.omdbapi.com/?apikey=68b7a486${urlTitle}${urlYear}`)
    .then(resp => {
      const movies = resp.data.Search

      dispatch({ type: 'CLEAN' })

      movies.forEach(({ imdbID }) => {
        axios.get(`http://www.omdbapi.com/?apikey=68b7a486&i=${imdbID}&plot=full&tomatoes=true`)
          .then(resp => {
            const { Title, Actors, Awards, Country, Director, Genre, Year, Language, Runtime, Writer, Type, Poster, Ratings, imdbID, BoxOffice } = resp.data

            let sum = 0
            Ratings.forEach(({ Value }) => {
              const imdb = Value.includes('/') ? Number(Value.split('/')[0]) : Number(Value.split('%')[0])
              sum = sum + (imdb < 10 ? imdb : imdb/10)
            })

            dispatch({
              type: 'ADD_MOVIE',
              payload: {
                title: Title,
                poster: Poster,
                rating: (sum/Ratings.length).toFixed(1),
                imdbID,
                actors: Actors,
                awards: Awards,
                country: Country,
                director: Director,
                genre: Genre,
                year: Year,
                languages: Language,
                runtime: Runtime,
                writer: Writer,
                type: Type,
                boxoffice: BoxOffice
              },
            })
          })
      })
    })
    .catch(err => alert("Busca Vazia!"))
}
