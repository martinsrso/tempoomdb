import React from 'react'
import GridListTileBar from '@material-ui/core/GridListTileBar';
import IconButton from '@material-ui/core/IconButton';
import InfoIcon from '@material-ui/icons/Info';
import GridListTile from '@material-ui/core/GridListTile';

const MovieCardComponent = props => (
  <GridListTile key={props.index} style={{width: '250', height: '400', padding: '5px'}}>
    <a href={`#/movie?imdbID=${props.movie.imdbID}`}><img src={props.movie.poster} alt={props.movie.title} style={{width: '250px', height: '400px'}} /></a>
    <GridListTileBar
      title={props.movie.title}
      subtitle={<span>Rating: {props.movie.rating}</span>}
      actionIcon={
        <IconButton>
          <a href={`#/movie?imdbID=${props.movie.imdbID}`}><InfoIcon style={{color: 'rgba(255, 255, 255, 0.54)'}}/></a>
        </IconButton>
      }
    />
  </GridListTile>
)

export default MovieCardComponent
