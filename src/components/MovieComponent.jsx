import React from 'react';
import { Link } from 'react-router'
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const MovieComponent = props => {
  const movie = props.movies.filter(movie => movie.imdbID === props.location.query.imdbID)[0]

  return (
    <React.Fragment>
    { movie ?
      <Card >
        <CardContent>
          <div style={{ width: '100px', height: '400px' }}>
            <img src={movie.poster} style={{ height: 'inherit' }}/>
          </div>
          <Typography variant="h5" component="h2">
            <br/>
            {movie.title} - IMDB: {movie.imdbID} - {movie.runtime}
          </Typography>
          <Typography color="textSecondary">
            Atores: {movie.actors}
          </Typography>
          <Typography component="p">
            <br />
              Prêmios: {movie.awards}
            <br />
          </Typography>
          { movie.boxoffice ?
            <Typography>
              <br />
                Bilheteria: {movie.boxoffice}
              <br />
            </Typography>
          : <p></p> }

          { movie.production ?
            <Typography>
              <br />
                Produção: {movie.production}
              <br />
            </Typography>
          : <p></p> }
        </CardContent>
        <CardActions>
          <Button size="small"><Link to={{pathname: "search"}}>Voltar</Link></Button>
        </CardActions>
      </Card>
    : <h1>Filme não encontrado</h1>
     }
    </React.Fragment>
  )
}

export default MovieComponent
