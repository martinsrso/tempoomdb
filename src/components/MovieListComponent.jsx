import React from 'react';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import ListSubheader from '@material-ui/core/ListSubheader';

import MovieCardComponent from './MovieCardComponent'

const MovieListComponent = props => (
  <div style={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'space-around', overflow: 'hidden'}}>
      <GridList cellHeight={380} style={{width: '200', height: '450', paddingTop: '20px'}}>
        {props.movies.map((movie, index) => (
          <MovieCardComponent key={index} index={index} movie={movie}/>
        ))}
      </GridList>
  </div>
)

export default MovieListComponent
