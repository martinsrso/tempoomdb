import React, { Fragment } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

import MovieListComponent from '../components/MovieListComponent'

const disabled = (title) => {
  title == "" ? true : false
}

const SearchComponent = props => (
  <React.Fragment>
    <div style={{flexGrow: '1'}}>
      <AppBar position="static" color="default">
        <Toolbar >
          <Typography style={{marginRight: '230px' }}  variant="h6" color="inherit">
            Lista de Filmes
          </Typography>
          <Typography variant="h6">
            Título: <input style={{height: '25px',  marginRight: '20px'}} type="text" value={props.title} onChange={e => props.changeTitle(e.target.value)} />
          </Typography>
          <Typography variant="h6">
              Ano: <input style={{height: '25px', marginRight: '20px'}} type="number" value={props.year} onChange={e => props.changeYear(e.target.value)} />
          </Typography>
          <Button disabled={!props.title} style={{ marginRight: '20px'}} variant="contained" color="primary" onClick={props.search}>
            Buscar
          </Button>
          <Button disabled={!props.title} color="secondary" variant="contained" style={{marginRight: '20px'}} onClick={() => props.sort(["alfabetica", !props.sortAttr[1], props.sortAttr[2]])}>
            Ordernar Alfabetico
          </Button>
          <Button disabled={!props.title} color="secondary" variant="contained" style={{marginRight: '20px'}} onClick={() => props.sort(["rating", props.sortAttr[1], !props.sortAttr[2]])}>
            Ordenar Rating
          </Button>
        </Toolbar>
      </AppBar>
    </div>
    <MovieListComponent movies={props.movies}/>
  </React.Fragment>
)

export default SearchComponent
