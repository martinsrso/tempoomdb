import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as actionCreators from '../actions/actions';
import SearchComponent from '../components/SearchComponent';

/**
 * Mapeando estado do redux para a props do SearchComponent
 * @type {function}
 * @param  {object} state Estado do redux
 * @return {object}       Propriedades com os nomes alterados a serem mapeadas ao componente
 */
const mapStateToProps = state => ({
  title: state.title,
  year: state.year,
  movies: state.movies,
  sortAttr: state.sortAttr
})

/**
 * Mapeando Action Creators para a props do SearchComponent
 * @param {function} dispatch Dispacha uma action. A única maneira de atualizar o estado do redux
 * @return  {object}          Único objeto com as Action Creators
 */
const mapDispatchToProps = dispatch => bindActionCreators({ ...actionCreators }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(SearchComponent);
