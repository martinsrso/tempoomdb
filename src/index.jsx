import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware, compose } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';

import Routes from './routes';
import reducers from './reducers/reducers';
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

/**
 * Aplicação disponível para ser renderizada
 * @constant
 * @type {object}
 */
const AppToRender = (
  /**
   * Provider Disponibiliza a camada de estado à aplicação
   * store - Estado da aplicação
   * createStore - Criar estado do redux, recebe os reducers combinados
   * composeEnhancers - O compose executa funções da direita para esquerda passando o retorno como parâmetro
   * applyMiddleware() - Para aplicar middlewares ao redux
   */
  <Provider store={createStore(reducers, composeEnhancers(applyMiddleware(thunk)))}>
    <Routes />
  </Provider>
);

ReactDOM.render(AppToRender, document.getElementById('app'));
