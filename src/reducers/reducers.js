const INITIAL_STATE = {
  title: '',
  year: '',
  movies: [],
  sortAttr: ["alfabetica", false, true] //ordenar TIPO, Ascendencia Alfabetica, Ascendencia Rating
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'CHANGE_TITLE':
      return { ...state, title: action.payload }
    case 'CHANGE_YEAR':
      return { ...state, year: action.payload }
    case 'ADD_MOVIE':
      return { ...state, movies: [...state.movies, action.payload] }
    case 'CLEAN':
      return { ...state, movies: [] }
    case 'SORT':
      return { ...state, sortAttr: action.payload.sortAttr }
    default:
      return state;
  }
};
