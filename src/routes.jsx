import React from 'react';
import { Router, Route, Redirect, hashHistory } from 'react-router';

import Search from './containers/SearchContainer';
import Movie from './containers/MovieContainer';

/** @type {function} Constroe as rodas das páginas da aplicação */
const Routes = () => (
  <Router history={hashHistory}>
    <Route path="/" component={Search} />
    <Route path="/search" component={Search} />
    <Route path="/movie" component={Movie} />
    <Redirect from="*" to="/" />
  </Router>
);

export default Routes;
