// test/utils/mockStore.js
import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import { sort } from "../../src/actions/actions";

export const mockStore = configureMockStore([thunk]);


describe("changePurchaseStatus", () => {
  it("handles changing a purchase status and fetches all purchases", async () => {
    const store = mockStore();
    await store.dispatch(sort([
      'alfabetica',
      false,
      true
    ]));
    const actions = store.getActions();
    expect(actions[0].toEqual({type: "SORT"}))
  });
});
