const path = require('path');

const HtmlPlugin = require('html-webpack-plugin');

module.exports = {
  devtool: 'source-map', // Ajuda no debug pelo navegador, apresentando alguns erros no código do projeto

  entry: ['babel-polyfill', path.join(__dirname, 'src', 'index.jsx')], // Arquivo inicial de entrada, a partir daqui toda a aplicação é carregada

  output: {
    filename: '[name].js', // Nome gerado automáticamente
  },

  devServer: { // Servidor de desenvolvimento
    disableHostCheck: true,
    compress: true,
    host: '0.0.0.0',
    port: 3000,
  },

  resolve: {
    extensions: ['.js', '.jsx'], // As extensões que serão interpretadas pelo webpack
  },

  plugins: [
    new HtmlPlugin({ // Gera o html de forma dinâmica
      title: 'Filme', // Título a ser exibido na página
      template: path.join(__dirname, 'src', 'template.html'), // Template no padrão ejs para gerar o html de forma dinâmica
    }),
  ],

  module: {
    rules: [{
      test: /.js[x]?$/, // Configura os arquivos a serem lidos pelo babel
      loader: 'babel-loader', // Babel ajuda a interpretar o código
      exclude: /node_modules|bower_components/, // Ignorar as pastas
      query: {
        presets: [
          'es2015', // Usado para compilar ES6 para ES5
          'react', // Usado para compilar react(JSX) para JavaScript
        ],
        plugins: ['transform-object-rest-spread'], // Permite a utilização de Spread "..."
      },
    }],
  },
};
